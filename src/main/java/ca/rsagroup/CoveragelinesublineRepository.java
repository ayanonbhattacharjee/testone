package ca.rsagroup;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoveragelinesublineRepository extends CrudRepository<Coveragelinesubline, Integer> {

}
