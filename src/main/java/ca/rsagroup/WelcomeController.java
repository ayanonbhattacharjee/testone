package ca.rsagroup;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.activation.DataSource;
import javax.annotation.PostConstruct;

import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.digester.Digester;
import org.apache.commons.digester.xmlrules.DigesterLoader;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

@RestController
public class WelcomeController {

	@RequestMapping("/helloworld")
	public String home() {
		return "Hello World!";
	}

	private static final String COMMON_TO_LOC = "D:\\ECM_E2E_TEST\\";
	private static final String COMMON_FROM_LOC = "D:\\ECM_E2E_TEST\\error-scenarios\\";

	private static final String AUD_CMN_TO = "audatex\\inbound";
	private static final String CPRO_CMN_TO = "claimspro\\inbound";
	private static final String CPRT_CMN_TO = "copart\\inbound";
	private static final String CFWD_CMN_TO = "crawford\\inbound";
	private static final String CNHM_CMN_TO = "cunningham_lindsay\\inbound";
	private static final String ECR_CMN_TO = "enterprise_car_rental\\inbound";

	private static enum ERRORTYPE {
		QRCLM, ONBS, VNDINV, UNZP, MTDT, DUPL;
	};

	private class FileLocations {
		String toLoc;
		String fromLoc;

		String getToLoc() {
			return toLoc;
		}

		void setToLoc(String toLoc) {
			this.toLoc = toLoc;
		}

		String getFromLoc() {
			return fromLoc;
		}

		void setFromLoc(String fromLoc) {
			this.fromLoc = fromLoc;
		}

		@Override
		public String toString() {
			return "FileLocations [toLoc=" + toLoc + ", fromLoc=" + fromLoc + "]";
		}

	}

	private Map<ERRORTYPE, List<FileLocations>> map = new HashMap<ERRORTYPE, List<FileLocations>>();

	@PostConstruct
	public void beanOnLoad() {
		FileLocations fileLocations = null;
		List<FileLocations> fileLocationList = null;

		fileLocationList = new ArrayList<FileLocations>();
		map.put(ERRORTYPE.QRCLM, fileLocationList);
		fileLocations = new FileLocations();
		fileLocations.setToLoc(AUD_CMN_TO);
		fileLocations.setFromLoc("qrclm-error\\aud");
		fileLocationList.add(fileLocations);

		fileLocations = new FileLocations();
		fileLocations.setToLoc(CPRO_CMN_TO);
		fileLocations.setFromLoc("qrclm-error\\cpro");
		fileLocationList.add(fileLocations);

		fileLocations = new FileLocations();
		fileLocations.setToLoc(CPRT_CMN_TO);
		fileLocations.setFromLoc("qrclm-error\\cprt");
		fileLocationList.add(fileLocations);

		fileLocations = new FileLocations();
		fileLocations.setToLoc(CFWD_CMN_TO);
		fileLocations.setFromLoc("qrclm-error\\cfwd");
		fileLocationList.add(fileLocations);

		fileLocations = new FileLocations();
		fileLocations.setToLoc(CNHM_CMN_TO);
		fileLocations.setFromLoc("qrclm-error\\cnhm");
		fileLocationList.add(fileLocations);

		fileLocations = new FileLocations();
		fileLocations.setToLoc(ECR_CMN_TO);
		fileLocations.setFromLoc("qrclm-error\\ecr");
		fileLocationList.add(fileLocations);

		fileLocationList = new ArrayList<FileLocations>();
		map.put(ERRORTYPE.ONBS, fileLocationList);
		fileLocations = new FileLocations();
		fileLocations.setToLoc(AUD_CMN_TO);
		fileLocations.setFromLoc("onbs-error\\aud");
		fileLocationList.add(fileLocations);

		fileLocations = new FileLocations();
		fileLocations.setToLoc(CPRO_CMN_TO);
		fileLocations.setFromLoc("onbs-error\\cpro");
		fileLocationList.add(fileLocations);

		fileLocations = new FileLocations();
		fileLocations.setToLoc(CPRT_CMN_TO);
		fileLocations.setFromLoc("onbs-error\\cprt");
		fileLocationList.add(fileLocations);

		fileLocations = new FileLocations();
		fileLocations.setToLoc(CFWD_CMN_TO);
		fileLocations.setFromLoc("onbs-error\\cfwd");
		fileLocationList.add(fileLocations);

		fileLocations = new FileLocations();
		fileLocations.setToLoc(CNHM_CMN_TO);
		fileLocations.setFromLoc("onbs-error\\cnhm");
		fileLocationList.add(fileLocations);

		fileLocations = new FileLocations();
		fileLocations.setToLoc(ECR_CMN_TO);
		fileLocations.setFromLoc("onbs-error\\ecr");
		fileLocationList.add(fileLocations);

		fileLocationList = new ArrayList<FileLocations>();
		map.put(ERRORTYPE.VNDINV, fileLocationList);
		fileLocations = new FileLocations();
		fileLocations.setToLoc(AUD_CMN_TO);
		fileLocations.setFromLoc("vndinv-error\\aud");
		fileLocationList.add(fileLocations);

		fileLocations = new FileLocations();
		fileLocations.setToLoc(ECR_CMN_TO);
		fileLocations.setFromLoc("vndinv-error\\ecr");
		fileLocationList.add(fileLocations);

		fileLocationList = new ArrayList<FileLocations>();
		map.put(ERRORTYPE.UNZP, fileLocationList);
		fileLocations = new FileLocations();
		fileLocations.setToLoc(CPRO_CMN_TO);
		fileLocations.setFromLoc("unzp-error\\cpro");
		fileLocationList.add(fileLocations);

		fileLocations = new FileLocations();
		fileLocations.setToLoc(CFWD_CMN_TO);
		fileLocations.setFromLoc("unzp-error\\cfwd");
		fileLocationList.add(fileLocations);

		fileLocations = new FileLocations();
		fileLocations.setToLoc(CNHM_CMN_TO);
		fileLocations.setFromLoc("unzp-error\\cnhm");
		fileLocationList.add(fileLocations);

		fileLocations = new FileLocations();
		fileLocations.setToLoc(ECR_CMN_TO);
		fileLocations.setFromLoc("unzp-error\\ecr");
		fileLocationList.add(fileLocations);

		fileLocationList = new ArrayList<FileLocations>();
		map.put(ERRORTYPE.MTDT, fileLocationList);
		fileLocations = new FileLocations();
		fileLocations.setToLoc(CPRO_CMN_TO);
		fileLocations.setFromLoc("mtdt-error\\cpro");
		fileLocationList.add(fileLocations);

		fileLocations = new FileLocations();
		fileLocations.setToLoc(CFWD_CMN_TO);
		fileLocations.setFromLoc("mtdt-error\\cfwd");
		fileLocationList.add(fileLocations);

		fileLocations = new FileLocations();
		fileLocations.setToLoc(CNHM_CMN_TO);
		fileLocations.setFromLoc("mtdt-error\\cnhm");
		fileLocationList.add(fileLocations);

		fileLocations = new FileLocations();
		fileLocations.setToLoc(ECR_CMN_TO);
		fileLocations.setFromLoc("mtdt-error\\ecr");
		fileLocationList.add(fileLocations);

		fileLocationList = new ArrayList<FileLocations>();
		map.put(ERRORTYPE.DUPL, fileLocationList);
		fileLocations = new FileLocations();
		fileLocations.setToLoc(AUD_CMN_TO);
		fileLocations.setFromLoc("dupl-error\\aud");
		fileLocationList.add(fileLocations);

		fileLocations = new FileLocations();
		fileLocations.setToLoc(CPRO_CMN_TO);
		fileLocations.setFromLoc("dupl-error\\cpro");
		fileLocationList.add(fileLocations);

		fileLocations = new FileLocations();
		fileLocations.setToLoc(CPRT_CMN_TO);
		fileLocations.setFromLoc("dupl-error\\cprt");
		fileLocationList.add(fileLocations);

		fileLocations = new FileLocations();
		fileLocations.setToLoc(CFWD_CMN_TO);
		fileLocations.setFromLoc("dupl-error\\cfwd");
		fileLocationList.add(fileLocations);

		fileLocations = new FileLocations();
		fileLocations.setToLoc(CNHM_CMN_TO);
		fileLocations.setFromLoc("dupl-error\\cnhm");
		fileLocationList.add(fileLocations);

		fileLocations = new FileLocations();
		fileLocations.setToLoc(ECR_CMN_TO);
		fileLocations.setFromLoc("dupl-error\\ecr");
		fileLocationList.add(fileLocations);
	}

	@RequestMapping(path = "/pumpdata", method = RequestMethod.POST, consumes = { MediaType.TEXT_XML_VALUE })
	public void pumpData(@RequestBody String errorType) {
		System.out.println(errorType);
		ERRORTYPE errtp = ERRORTYPE.valueOf(errorType);

		System.out.println(errtp.name());

		List<FileLocations> fileLocations = map.get(errtp);

		System.out.println(fileLocations);

		for (FileLocations fileLoc : fileLocations) {

			String fileFrom = COMMON_FROM_LOC + fileLoc.getFromLoc();
			String fileTo = COMMON_TO_LOC + fileLoc.getToLoc();

			System.out.println(fileFrom);
			System.out.println(fileFrom);

			File destDir = new File(fileTo);

			File file = new File(fileFrom);

			File[] files = file.listFiles();
			for (int i = 0; i < files.length; i++) {
				File srcFile = files[i];

				try {
					FileUtils.copyFileToDirectory(srcFile, destDir);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	//Collaborator Test Comments ##

	@Autowired
	CoveragelinesublineRepository coveragelinesublineRepository;

	@RequestMapping(path = "/xmlToData")
	public void xmlToData() {
		File file = new File(
				"C:/Users/527366/RSA_REPO/esb_policy_diamond/src/main/resources/xslt/lookup/lookup-coveragecode-line-subline-mapping.xml");

		InputStream ins = null;
		Digester digester = new Digester();

		digester.addObjectCreate("CoverageCodes", CoveragelinesublineHolder.class);
		digester.addObjectCreate("CoverageCodes/CoverageCode", Coveragelinesubline.class);
		digester.addSetProperties("CoverageCodes/CoverageCode", "code", "coverageCode");
		digester.addBeanPropertySetter("CoverageCodes/CoverageCode/Line", "coverageLine");
		digester.addBeanPropertySetter("CoverageCodes/CoverageCode/subLine", "coverageSubline");
		digester.addSetNext("CoverageCodes/CoverageCode", "addCoveragelinesubline");

		try {
			ins = new FileInputStream(file);

			CoveragelinesublineHolder coveragelinesublineHolder = (CoveragelinesublineHolder) digester.parse(ins);
			List<Coveragelinesubline> list = coveragelinesublineHolder.getCoveragelinesublines();
			System.out.println(list.size());
			System.out.println(list.get(0).getCoverageCode());
			coveragelinesublineRepository.save(list);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@RequestMapping(path = "/getCoveragelinesubline", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<CoveragelinesublineHolder> getCoveragelinesubline() {
		Iterable<Coveragelinesubline> iterable = null;
		List<Coveragelinesubline> coveragelinesublines = null;
		iterable = coveragelinesublineRepository.findAll();
		coveragelinesublines = (List<Coveragelinesubline>) IteratorUtils.toList(iterable.iterator());
		System.out.println(coveragelinesublines.size());
		CoveragelinesublineHolder coveragelinesublineHolder = new CoveragelinesublineHolder();
		coveragelinesublineHolder.setCoveragelinesublines(coveragelinesublines);
		return new ResponseEntity<CoveragelinesublineHolder>(coveragelinesublineHolder, HttpStatus.OK);
	}
}
