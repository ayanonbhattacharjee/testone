package ca.rsagroup;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="CoverageDetails")
@XmlAccessorType(XmlAccessType.FIELD)
public class CoveragelinesublineHolder {

	@XmlElement(name="Coverage")
	private List<Coveragelinesubline> coveragelinesublines;

	public CoveragelinesublineHolder() {
		coveragelinesublines = new ArrayList<Coveragelinesubline>();
	}

	public List<Coveragelinesubline> getCoveragelinesublines() {
		return coveragelinesublines;
	}

	public void setCoveragelinesublines(List<Coveragelinesubline> coveragelinesublines) {
		this.coveragelinesublines = coveragelinesublines;
	}

	public void addCoveragelinesubline(Coveragelinesubline coveragelinesubline) {
		coveragelinesublines.add(coveragelinesubline);
	}

}
